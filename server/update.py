from jinja2 import Template
import hashlib
from datetime import datetime
import os
import sys
import subprocess
from packaging import version

import yaml
import argparse

_Template = """
metadata:
  channel: {{ channel }}
  release: {{ release }}
  version: {{ version }}
  publisher: {{ publisher }}
  build: {{ build }}
spec:
  boots:
  {% for boot in boots %}
    - name: {{ boot.name }}
      sha256sum: {{ boot.sha256sum }}
      size: {{ boot.size }}
  {% endfor %}
  bundles:
  {% for bundle in bundles %}
    - name: {{ bundle.name }}
      sha256sum: {{ bundle.sha256sum }}
      size: {{ bundle.size }}
      {% if bundle.rdiffs %}
      rdiffs:
        {% for rdiff in bundle.rdiffs %}
        - origin: {{ rdiff.origin }}
          sha256sum: {{ rdiff.sha256sum }}
          size: {{ rdiff.size }}
        {% endfor-%}
      {% endif %}
  {% endfor %}
{% if config %}
config:
  {% if config.boot_options %}
  boot_options: {{ config.boot_options}}
  {% endif %}
  {% if config.minimum_device_size %}
  minimum_device_size: {{ config.minimum_device_size }}
  {% endif %}
  api_server: http://os.codemao.cn/api
  registry_mirror: http://os.codemao.cn/ota
{% endif %}
"""

class FileSystem():
    def __init__(self, repository, release, version=None, channel='catty', publisher="CodeOS <sysdev@codemao.cn>"):
        self.version = version
        self.channel = channel
        self.release = release
        self.repository = repository     

        self.publisher = publisher
        self.build = None

        self.boots = []
        self.bundles = []

        self.config = {"boot_options": "boot=live perch quiet splash nottyautologin from=%s/%s" % (self.channel, self.release), 
                        "minimum_device_size": 16}

        self.directory = os.path.join(self.repository, channel, release)
        if not os.path.exists(self.directory):
            raise OSError("%s is not exists" % self.directory)

    def progress(self, force=False):
        if force and os.path.exists(os.path.join(self.directory, 'release.yaml')):
            print("Remove release.yaml ...")
            os.unlink(os.path.join(self.directory, 'release.yaml'))
        if os.path.exists(os.path.join(self.directory, 'release.yaml')):
            print("Load %s release.yaml ..." % os.path.basename(self.directory))
            metadata = yaml.load(open(os.path.join(self.directory, 'release.yaml')))
            self.build = metadata['metadata']['build']
            self.boots = metadata['spec'].get('boots', [])
            self.bundles = metadata['spec'].get('bundles', [])
        else:
            print("Progressing release.yaml ...")
            self.build = datetime.now().strftime('%a, %b %d %H:%M')
            for file in os.listdir(self.directory):
                if file.startswith("vmlin") or file.startswith("initr"):
                    sha256sum = self.sha256sum(os.path.join(self.directory, file))
                    size = self.filesize(os.path.join(self.directory, file))
                    self.boots.append({'name': file, 'sha256sum': sha256sum, 'size': size})

                if file.startswith(self.channel):
                    for module in os.listdir(os.path.join(self.directory, self.channel)):
                        if module.endswith(".cb"):
                            sha256sum = self.sha256sum(os.path.join(self.directory, self.channel, module))
                            size = self.filesize(os.path.join(self.directory, self.channel, module))
                            self.bundles.append({'name': module, 'sha256sum': sha256sum, 'size': size})
    
    def gen_rdiff_sign(self, force=False):
        for bundle in self.bundles:
            bundlepath = os.path.join(self.directory, self.channel, bundle['name'])
            if force and os.path.exists(bundlepath + ".sign"):
                os.unlink(bundlepath + ".sign")
            if not os.path.exists(bundlepath + ".sign"):
                print("rdiff signature %s %s" % (bundlepath, bundlepath + ".sign"))
                subprocess.check_call("rdiff signature %s %s" % (bundlepath, bundlepath + ".sign"), shell=True)
    
    def save_yaml(self):
        template = Template(_Template, trim_blocks=True, lstrip_blocks=True)
        config = template.render(filesystem.dict)
        print(config)
        with open(os.path.join(self.directory, 'release.yaml'), 'w') as fp:    
            fp.write(config)

    def gen_rdiff_delta(self, origfs):
        if origfs.release == self.release:
            return

        if not os.path.exists(os.path.join(self.directory, 'rdiffs')):
            os.makedirs(os.path.join(self.directory, 'rdiffs'))
            
        for bundle in self.bundles:
            for origbundle in origfs.bundles:
                if bundle.get('rdiffs', None) is None:
                        bundle['rdiffs'] = []
                if bundle['name'] == origbundle['name']:
                    print("Generate rdiff from %s" % origbundle['name'])
                    if not os.path.exists(os.path.join(origfs.directory, origfs.channel, origbundle['name'] + ".sign")):
                        print(os.path.join(origfs.directory, origfs.channel, origbundle['name'] + ".sign"))
                        print(" x %s sign file not found." % origbundle['name']) 
                        sys.exit(1)

                    if os.path.exists(os.path.join(self.directory, 'rdiffs', '%s.temp' % bundle['name'])):
                        os.unlink(os.path.join(self.directory, 'rdiffs', '%s.temp' % bundle['name']))

                    subprocess.check_output("rdiff delta %s %s %s" % (
                        os.path.join(origfs.directory, origfs.channel, origbundle['name'] + ".sign"),
                        os.path.join(self.directory, self.channel, bundle['name']),
                        os.path.join(self.directory, 'rdiffs', '%s.temp' % bundle['name'])
                        ),shell=True)
                    if not os.path.exists(os.path.join(self.directory, 'rdiffs', '%s.temp' % bundle['name'])):
                        print(" x Generate rdiff failed")
                        sys.exit(1)
                    sha256sum = self.sha256sum(os.path.join(self.directory, 'rdiffs', '%s.temp' % bundle['name']))
                    size = self.filesize(os.path.join(self.directory, 'rdiffs', '%s.temp' % bundle['name']))
                    os.rename(
                        os.path.join(self.directory, 'rdiffs', '%s.temp' % bundle['name']), 
                        os.path.join(self.directory, 'rdiffs', sha256sum)
                        )
                    
                    _already_gen = False
                    for rdiff in bundle['rdiffs']:
                        if str(rdiff['origin']) == str(origfs.release):
                            rdiff['sha256sum'] = sha256sum
                            rdiff['size'] = size
                            _already_gen = True
                            break

                    if not  _already_gen:
                        bundle['rdiffs'].append({'origin': origfs.release, 'sha256sum': sha256sum, 'size': size})

    @staticmethod
    def filesize(filename):
        return os.stat(filename).st_size

    @staticmethod
    def sha256sum(filename, block_size=65536):
        sha256 = hashlib.sha256()
        with open(filename, 'rb') as f:
            while True:
                block = f.read(block_size)
                if not block:
                    break
                sha256.update(block)
        return sha256.hexdigest()

    @property
    def dict(self):
        return {'version': self.version, 'channel': self.channel, 'release': self.release, 'publisher': self.publisher,
            'boots': self.boots, 'bundles': self.bundles, 'config': self.config, 'build': self.build
            }

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--repo', default="/srv/repo")
    parser.add_argument('--channel', default="catty")
    parser.add_argument('--release', required=True)
    parser.add_argument('--version', default="nightly")
    parser.add_argument('--rdiffs', nargs='*')
    parser.add_argument('--total', type=int, default=4)
    parser.add_argument('--force', action='store_true')
    args = parser.parse_args()
    filesystem = FileSystem(repository=args.repo, release=args.release, 
        channel=args.channel, version=args.version)

    if args.rdiffs:
        rdiffs = args.rdiffs
    else:
        rdiffs = os.listdir(os.path.join(args.repo, args.channel))
        rdiffs = sorted(list(filter(lambda x: x not in ['current', 'misc', args.release] and version.parse(x) < version.parse(args.release), rdiffs)))
        rdiffs = rdiffs[0-args.total:]

    print("Generating filesystem: %s/%s" % (args.channel, args.release))
    print("    version: %s" % args.version)
    print(" and will generate rdiffs: %s" % " ".join(rdiffs))

    filesystem.progress(force=args.force)
    filesystem.gen_rdiff_sign(force=args.force)

    for origrel in rdiffs:
        origfs = FileSystem(repository=args.repo, release=origrel, channel=args.channel)
        origfs.progress()
        filesystem.gen_rdiff_delta(origfs)

    filesystem.save_yaml()
