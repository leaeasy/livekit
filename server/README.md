Static-and-Rest Server
======================================

## Routes
Get: /channel/:channel     - display a list of all release channels
Get: /channel/:channel/:id - show an channel release

## Get information for update with POST
curl -X POST -H  --data-binary @/tmp/repos/catty/20180717/release.yaml http://127.0.0.1:8888/api/update
