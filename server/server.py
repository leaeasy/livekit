import tornado.ioloop
import tornado.web
from tornado import gen

from collections import OrderedDict
import time

import traceback
import os
import yaml
import json
import time

import queue


class Cache():
    def __init__(self, max_size=30, timeout=None):
        self._store = OrderedDict()
        self._max_size = max_size
        self._timeout = timeout

    def set(self, key, value, timeout=None):
        self._check_limit()
        if not timeout:
            timeout = self._timeout
        if timeout:
            timeout = time.time() + timeout

        self._store[key] = (value, timeout)

    def get(self, key, default=None):
        data = self._store.get(key)
        if not data:
            return default
        
        value, expire = data
        if expire and time.time() > expire:
            del self._store[key]
            return default
        return value

    def _check_limit(self):
        if len(self._store) >= self._max_size:
            self._store.popitem(last=False)

    def clear(self):
        self._store = OrderedDict()

kRepoPath = "/srv/repo"
kRepoURL = "http://os.codemao.cn/repo"
kChannel = "catty"
kCache = Cache(max_size=50)

class ApplicationException(tornado.web.HTTPError):
    pass


class ApplicationBaseHandler(tornado.web.RequestHandler):

    def write_error(self, status_code, **kwargs):

        self.set_header('Content-Type', 'application/json')
        if self.settings.get("serve_traceback") and "exc_info" in kwargs:
            # in debug mode, try to send a traceback
            lines = []
            for line in traceback.format_exception(*kwargs["exc_info"]):
                lines.append(line)
            self.finish(json.dumps({
                'error': {
                    'code': status_code,
                    'message': self._reason,
                    'traceback': lines,
                }
            }))
        else:
            self.finish(json.dumps({
                'error': {
                    'code': status_code,
                    'message': self._reason,
                }
            }))


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
                (r"/api/channel/(?P<channel>.*)/(?P<date>.*)", ChannelHandler),
                (r"/api/channel/(?P<channel>.*)", ChannelsHandler),
                (r"/api/update", UpdateHandler)
                ]
        super(Application, self).__init__(handlers, autoreload=True)

class ChannelHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self, channel, date):
        yamlfile = os.path.join(kRepoPath, channel, date, 'release.yaml')
        if not os.path.exists(yamlfile):
            raise tornado.web.HTTPError(status_code=404, reason="Invalid resource path")
        
        metadata = {}
        with open(yamlfile) as fp:
            metadata = yaml.load(fp, Loader=yaml.BaseLoader)
        
        self.write(json.dumps(metadata))


class ChannelsHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self, channel):
        channel_path = os.path.join(kRepoPath, channel)
        releases = []
        current = None
        
        for release in os.listdir(channel_path):
            release_dir = os.path.join(channel_path, release)
            if release != "current":
                if os.path.isdir(release_dir) and os.path.exists(os.path.join(release_dir, "release.yaml")):
                    releases.append(release)
            else:
                if os.path.islink(release_dir) and os.path.exists(os.path.join(release_dir, "release.yaml")):
                    current = os.path.basename(os.path.realpath(release_dir))

        releases.sort(reverse=True)
        # use the first item as current if current symbol link not exists
        if current is None:
            current = releases[0]
        
        config = {
            "releases": releases,
            "current": current
        }
        
        self.write(json.dumps(config))

class UpdateHandler(ApplicationBaseHandler):

    def lastests_release(self, channel, default="current"):
        #TODO: we just choose the symbol link as newest release.
        lastest_release = None
        current = os.path.join(kRepoPath, str(channel), default)
        if os.path.islink(current):
            realpath = os.path.realpath(current)
            lastest_release = os.path.basename(realpath)
        
        return lastest_release


    def release_config(self, channel, release):
        yaml_config = os.path.join(kRepoPath, str(channel), str(release), 'release.yaml')
        config = None
        if os.path.exists(yaml_config):
            config = yaml.load(open(yaml_config))
        return config

    def module_rdiffs(self, metadata, module):
        rdiffs = []
        if metadata is None:
            return None
        for bundle in metadata['spec']['bundles']:
            if bundle['name'] == module:
                for rdiff in bundle.get('rdiffs', []):
                    rdiffs.append(rdiff['origin'])
        return rdiffs

    @staticmethod
    def show_rdiffs_path(parents, origin, dest):
        relation_paths = [origin]
        parent = parents[origin]
        while parent:
            relation_paths.append(parent)
            parent = parents[parent]
        
        result = {}
        while len(relation_paths) > 1:
            path = relation_paths.pop(0)
            result[path] = relation_paths[0]
        return result
    
    def fetch_rdiffs(self, channel, module, origin, dest):
        origin = int(origin)
        dest = int(dest)
        q = queue.Queue()
        parents = dict()
        parents[dest] = None
        q.put(dest)

        while not q.empty():
            current = q.get()
            _release = self.release_config(channel, current)
            if _release:
                rdiffs = self.module_rdiffs(_release, module)
                if rdiffs:
                    for rdiff in rdiffs:
                        parents[rdiff] = current
                        if rdiff == origin:
                            return self.show_rdiffs_path(parents, origin, dest)
                        q.put(rdiff)

        return False

    def get_lastest_metadata(self, channel=kChannel):
        lastest_release = self.lastests_release(channel)
        if lastest_release is None:
            raise ApplicationException(reason="None of lastest release found", status_code=500)

        lastest_yaml = os.path.join(kRepoPath, channel, str(lastest_release), 'release.yaml')
        lastest_meta = yaml.load(open(lastest_yaml))
        return lastest_meta

    def post(self):
        if 'Content-Type' in list(self.request.headers.keys()) and \
            self.request.headers['Content-Type'].startswith("text/x-yaml"):
            body = yaml.load(self.request.body)
        else:
            raise ApplicationException(reason="Wrong content-type found", status_code=500)
        
        if body is None:
            lastest_meta = self.get_lastest_metadata()
            return self.write(yaml.dump(self.remove_bundle_pdiffs(lastest_meta), default_flow_style=False))
        
        body_meta = body.get('metadata', None)
        if body_meta is None:
            lastest_meta = self.get_lastest_metadata()
            return self.write(yaml.dump(self.remove_bundle_pdiffs(lastest_meta), default_flow_style=False))

        channel = body_meta.get('channel', None)
        release = body_meta.get('release', None)

        if channel is None:
            lastest_meta = self.get_lastest_metadata()
            return self.write(yaml.dump(self.remove_bundle_pdiffs(lastest_meta), default_flow_style=False))

        lastest_meta = self.get_lastest_metadata(channel=channel)
        if release is None:
            return self.write(yaml.dump(self.remove_bundle_pdiffs(lastest_meta), default_flow_style=False))

        # we don't trust the user post data, reload from repository.
        origin_yaml = os.path.join(kRepoPath, channel, str(release), 'release.yaml')
        if not os.path.exists(origin_yaml):
            return self.write(yaml.dump(self.remove_bundle_pdiffs(lastest_meta), default_flow_style=False))

        lastest_release = lastest_meta['metadata']['release']

        origin_meta = yaml.load(open(origin_yaml))
        # we try to fetch the result from kCache
        cache_key = "%s-%s-%s" % (channel, release, lastest_meta)
        if kCache.get(cache_key):
            return self.write(yaml.dump(kCache.get(cache_key), default_flow_style=False))
            
        diffpathes = {}
        for bundle in lastest_meta['spec']['bundles']:
            bundle_name = bundle['name']
            diffpathes[bundle_name] = self.fetch_rdiffs(channel, bundle_name, release, lastest_release)

        bundles = []
        for bundle in lastest_meta['spec']['bundles']:
            bundle_name = bundle['name']
            _bundle = {'name': bundle_name, 'sha256sum': bundle['sha256sum']}
            local = False
            for orig_bundle in origin_meta['spec']['bundles']:
                if orig_bundle['name'] == bundle['name']:
                    if orig_bundle['sha256sum'] == bundle['sha256sum']:
                        _bundle.update({'method': 'local'})
                        local = True
                        break

            if local is False:
                if diffpathes.get(bundle_name):
                    rdiffs = diffpathes[bundle_name]
                    origin_bundle = self.get_bundle(origin_meta, bundle_name)
                    _bundle.update({'method': 'patch'})
                    _bundle.update({'source': {'name': origin_bundle['name'], 'sha256sum': origin_bundle['sha256sum']}})
                    rdiffs_info = []
                    for rdiff in rdiffs:
                        dest_meta = yaml.load(open(os.path.join(kRepoPath, channel, str(rdiffs[rdiff]), 'release.yaml')))
                        rdiff_info = self.get_rdiff(dest_meta, bundle_name, rdiff)
                        rdiff_info['origin'] = dest_meta['metadata']['release']
                        rdiffs_info.append(rdiff_info)

                    _bundle.update({'rdiffs': rdiffs_info})
                else:
                    if not _bundle.get('method'):
                        _bundle.update({'method': 'remote'})
                    
            bundles.append(_bundle)
        
        update_meta = {}
        for key in lastest_meta:
            update_meta[key] = lastest_meta[key]

        update_meta['spec'].update({'bundles': bundles})
        if not update_meta.get('install'):
            update_meta['install'] = {}

        boots = []

        lastest_boots = []
        origin_boots = []
        for boot in lastest_meta['spec']['boots']:
            lastest_boots.append(boot['sha256sum'])
    
        for boot in origin_meta['spec'].get('boots', []):
            origin_boots.append(boot['sha256sum'])

        if sorted(lastest_boots) == sorted(origin_boots):
            update_meta['install'].update({'update_kernel': False})
        else:
            update_meta['install'].update({'update_kernel': True})

        update_meta['metadata'].update({'manipulate': 'update', 'origin': release})
        kCache.set(cache_key, update_meta)

        self.write(yaml.dump(update_meta, default_flow_style=False))
    
    @staticmethod
    def remove_bundle_pdiffs(metadata):
        result = dict(metadata)
        result['metadata'].update({'manipulate': 'install'})
        bundles = []
        for bundle in result['spec']['bundles']:
            if 'rdiffs' in bundle:
                del bundle['rdiffs']
            bundle.update({'method': 'remote'})

        return result

    @staticmethod
    def get_bundle(metadata, bundle_name):
        for bundle in metadata['spec']['bundles']:
            if bundle['name'] == bundle_name:
                return bundle
        return None
    
    @staticmethod
    def get_rdiff(metadata, bundle_name, origin):
        for bundle in metadata['spec']['bundles']:
            if bundle['name'] == bundle_name:
                if bundle.get('rdiffs'):
                    rdiffs = bundle['rdiffs']
                    for rdiff in rdiffs:
                        if rdiff['origin'] == origin:
                            return rdiff
        return None

if __name__ == "__main__":
    Application().listen(8000)
    tornado.ioloop.IOLoop.instance().start()
