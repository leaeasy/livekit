CodeOS系统及其自动更新
=======================
系统安装磁盘只支持gpt

## 内核功能
需要开启支持
* aufs
* zram
* f2fs
* ext4

## 系统分区
通过PARTLABEL区分系统分区
- boot
- system
- data
- recovery
- misc

### 系统分区详细解析
#### boot
使用bootctl引导
* vmlinuz
* initrd
* EFI/*

#### system
* filesystem.yaml
* 01-basesystem.sqfs
* 02-applications.sqfs
* 03-flatpak.sqfs

filesystem.yaml: 系统模块列表、校验和、系统更新等信息

#### recovery
占位分区

#### data
用户产生文件， 删除后或者添加.nouse不自动联合挂载

#### misc
占位分区
