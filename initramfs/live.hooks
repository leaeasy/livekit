#!/bin/sh
. /usr/share/initramfs-tools/hook-functions

modules="$modules ext2 ext3 ext4 squashfs f2fs aufs"
modules="$modules af_packet atkbd i8042 psmouse crc32"
modules="$modules vfat nls_cp437 nls_iso8859-1 nls_utf8"
modules="$modules i915 zram loop"

# Include most USB host and dual-role drivers
copy_modules_dir kernel/drivers/usb/host \
	hwa-hc.ko sl811_cs.ko sl811-hcd.ko \
	u132-hcd.ko whci-hcd.ko
copy_modules_dir kernel/drivers/usb/c67x00
copy_modules_dir kernel/drivers/usb/chipidea
copy_modules_dir kernel/drivers/usb/dwc2
copy_modules_dir kernel/drivers/usb/dwc3
copy_modules_dir kernel/drivers/usb/isp1760
copy_modules_dir kernel/drivers/usb/musb
copy_modules_dir kernel/drivers/usb/renesas_usbhs

# Include all HID drivers unless we're sure they
# don't support keyboards.  hid-*ff covers various
# game controllers with force feedback.
copy_modules_dir kernel/drivers/hid \
	'hid-*ff.ko' hid-a4tech.ko hid-cypress.ko \
	hid-dr.ko hid-elecom.ko hid-gyration.ko \
	hid-icade.ko hid-kensington.ko hid-kye.ko \
	hid-lcpower.ko hid-magicmouse.ko \
	hid-multitouch.ko hid-ntrig.ko \
	hid-petalynx.ko hid-picolcd.ko hid-pl.ko \
	hid-ps3remote.ko hid-quanta.ko \
	'hid-roccat-ko*.ko' hid-roccat-pyra.ko \
	hid-saitek.ko hid-sensor-hub.ko hid-sony.ko \
	hid-speedlink.ko hid-tivo.ko hid-twinhan.ko \
	hid-uclogic.ko hid-wacom.ko hid-waltop.ko \
	hid-wiimote.ko hid-zydacron.ko

# Any of these might be needed by other drivers
copy_modules_dir kernel/drivers/bus
copy_modules_dir kernel/drivers/clk
copy_modules_dir kernel/drivers/gpio
copy_modules_dir kernel/drivers/i2c/busses
copy_modules_dir kernel/drivers/i2c/muxes
copy_modules_dir kernel/drivers/regulator
copy_modules_dir kernel/drivers/usb/phy

# Needed for periodic fsck
copy_modules_dir kernel/drivers/rtc

copy_modules_dir kernel/drivers/ata
copy_modules_dir kernel/drivers/mmc
copy_modules_dir kernel/drivers/block
copy_modules_dir kernel/drivers/nvme
copy_modules_dir kernel/drivers/usb/storage

manual_add_modules $modules

copy_exec /sbin/swapon
copy_exec /sbin/mkswap

copy_exec /usr/lib/livekit/bin/mkfs.f2fs /bin/mkfs.f2fs
copy_exec /usr/lib/livekit/bin/blkid /sbin/blkid
copy_exec /usr/lib/livekit/bin/yq /bin/yq
