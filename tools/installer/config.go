package main

type Configuration struct {
	Metadata Metadata
	Config   map[string]string
	Spec     Spec
}

type Metadata struct {
	Channel    string
	Release    string
	Version    string
	Build      string
	Publisher  string
}

type Spec struct {
	Boots   []*Boot   `yaml:"boots"`
	Bundles []*Bundle `yaml:"bundles"`
}

type Bundle struct {
	Name      string
	Sha256sum string
}

type Boot struct {
	Name      string
	Sha256sum string
}
