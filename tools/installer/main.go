package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"

	"github.com/akamensky/argparse"
	"github.com/fatih/color"
	"launchpad.net/goyaml"
)

const (
	kLive    = "/run/initramfs/data"
	kTarget  = "/target"
	kVersion = "1.0"
)

func main() {
	color.Green("CodeOS Installer - %s", kVersion)
	if _, err := os.Stat("/sys/firmware/efi/efivars"); err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}

	parser := argparse.NewParser("CodeOS Installer", "CodeOS OTA Updater Tools")

	yamlfile := parser.String("m", "metadata",
		&argparse.Options{Required: true, Help: "metadata is a required release.yaml file."})

	ratio := parser.Int("r", "ratio",
		&argparse.Options{Required: false, Help: "set the root partition ratio [1..100]"})

	policy := parser.String("p", "policy",
		&argparse.Options{
			Required: false,
			Help:     "set partition policy.\n Like: boot:efi:1Mib:512Mib;system:f2fs:512Mib:20480Mib;data:f2fs:20480Mib:100%}",
		})

	forceFormat := parser.Flag("f", "forcefomart", &argparse.Options{Help: "Force format disk to install filesystem"})

	if err := parser.Parse(os.Args); err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}

	var conf Configuration
	bytes, err := ioutil.ReadFile(*yamlfile)
	if err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}

	yamlPath, _ := filepath.Abs(*yamlfile)
	cachePath := filepath.Dir(yamlPath)

	goyaml.Unmarshal(bytes, &conf)
	device, err := SelectDevice()
	if err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}
	if device == "" {
		color.Red("None of device selected")
		os.Exit(1)
	}

	color.Green("Selected " + device + " as installation.")
	color.White("Cache path: " + cachePath)

	minimumDeviceSize, _ := strconv.ParseInt(conf.Config["minimum_device_size"], 10, 64)

	deviceSize, err := getDeviceSize(device)
	if err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}

	color.Yellow("Check the device size ...")
	if deviceSize < minimumDeviceSize*1024 {
		color.Red("Device Size is less than minium size")
		os.Exit(1)
	} else {
		color.Green("Check device size ok")
	}

	var partitionPolicy string
	if *policy != "" {
		partitionPolicy = *policy
	} else if *ratio != 0 {
		rootPartionEnd := deviceSize*int64(*ratio)/100 + 512
		partitionPolicy = fmt.Sprintf("boot:efi:1Mib:512Mib;system:f2fs:512Mib:%dMib;data:f2fs:%dMib:100%%", rootPartionEnd, rootPartionEnd)
	} else {
		color.Red("need -p / -r as argument")
		os.Exit(1)
	}
	color.Blue("Partition Policy - " + partitionPolicy)
	var choice string
	color.Cyan("Will install the filesystem: y/n")
	fmt.Scan(&choice)

	switch choice {
	case "Y", "y":
		{
			color.White(":: Starting installation ...")
		}
	default:
		{
			color.Red("Abort the installation.")
			os.Exit(1)
		}
	}

	CheckFilesystem(conf, cachePath)

	if *forceFormat {
		color.Yellow("Force format device")
	}

	if *forceFormat || !CheckPartitionsPrepared(device) {
		color.Yellow(":: format device...")
		if err := PreparePartitions(device, partitionPolicy); err != nil {
			color.Red("Prepare partitions error: " + err.Error())
			os.Exit(1)
		}
	}

	MountPartitions(device)
	InstallFilesystem(conf, cachePath)
	InstallBootLoader(conf)
	color.Green("Install CodeOS " + conf.Metadata.Channel + "-" + conf.Metadata.Release + " done.")
}
