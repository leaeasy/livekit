package main

import (
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	//"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/fatih/color"
	"launchpad.net/goyaml"
)

func PathExist(path string) bool {
	_, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}

func Sha256File(filePath string) (string, error) {
	color.White("Calculating checksums for " + filePath)
	var hashValue string
	file, err := os.Open(filePath)
	if err != nil {
		return hashValue, err
	}
	defer file.Close()

	reader := bufio.NewReaderSize(file, 8*1024*1024)
	hash := sha256.New()
	if _, err := io.Copy(hash, reader); err != nil {
		return hashValue, err
	}
	hashInBytes := hash.Sum(nil)
	hashValue = hex.EncodeToString(hashInBytes)
	return hashValue, nil
}

func Mkfs(path string, fstype string) error {
	color.White("mkfs " + path + " with " + fstype)
	argArr := make([]string, 0)
	if err := runCmd("wipefs -a " + path, true); err != nil {
		return err
	}
	switch fstype {
	case "fat32", "efi":
		{
			argArr = append(argArr, "mkfs.vfat", "-F32", path)
		}
	case "ext2", "ext3", "ext4":
		{
			argArr = append(argArr, "mkfs", "-t", fstype, "-F", path)
		}
	default:
		{
			argArr = append(argArr, "mkfs", "-t", fstype, path)
		}
	}
	//cmd := exec.Command(argArr[0], argArr[1:]...)
	var cmd string
	cmd = strings.Join(argArr, " ")
	if err := runCmd(cmd, true); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func CopyFile(source string, destination string) error {
	color.White("copying " + source + " to " + destination)
	reader, err := os.Open(source)

	if err != nil {
		return err
	}

	defer reader.Close()

	if err := os.MkdirAll(filepath.Dir(destination), os.ModePerm); err != nil {
		return err
	}

	writer, err := os.Create(destination)
	if err != nil {
		return err
	}

	defer writer.Close()

	_, err = io.Copy(writer, reader)
	if err != nil {
		return err
	}

	err = writer.Sync()
	if err != nil {
		return err
	}

	return nil
}

func SelectDevice() (path string, err error) {
	fileinfo, err := ioutil.ReadDir("/dev/disk/by-id/")
	if err != nil {
		return "", err
	}
	devices := map[string]struct{}{}
	for _, file := range fileinfo {
		if !strings.Contains(file.Name(), "-part") {
			relpath, _ := filepath.EvalSymlinks(
				filepath.Join("/dev/disk/by-id", file.Name()))
			devices[relpath] = struct{}{}
		}
	}
	i := 0
	selects := make(map[int]string)
	fmt.Println("Select install device: ")
	for path, _ := range devices {
		fmt.Println(i, ":", path)
		selects[i] = path
		i = i + 1
	}
	fmt.Scan(&i)
	return selects[i], nil
}

func getDeviceSize(device string) (size int64, err error) {
	command := fmt.Sprintf("blockdev --getsize64 %s", device)
	deviceSize, stderr, err := captureCmd(command, true)
	if err != nil {
		color.Red(err.Error() + stderr)
		os.Exit(1)
	}
	deviceSize = strings.Trim(deviceSize, "\n")
	size, err = strconv.ParseInt(deviceSize, 10, 64)
	if err != nil {
		return 0, err
	}
	size = size / 1048576
	return size, nil
}

func CheckPartitionsPrepared(device string) bool {
	color.Yellow("Check if partitions are prepared ...")
	for _, label := range []string{"boot", "system", "data"} {
		command := fmt.Sprintf("blkid -s PARTLABEL | grep '^%s' | grep -q 'PARTLABEL=\"%s\"'", device, label)
		if err := runCmd(command, true); err != nil {
			color.Cyan("cannot found label " + label)
			return false
		}
	}
	color.Green("Partitions are prepared ...")
	return true
}

func PreparePartitions(device string, partitionPolicy string) (err error) {
	color.Yellow("Start formating " + device)
	command := fmt.Sprintf("parted -s %s mktable gpt", device)
	if err := runCmd(command, true); err != nil {
		return err
	}

	var partPrefix string
	if strings.HasPrefix(device, "/dev/mmc") {
		partPrefix = fmt.Sprintf("%sp", device)
	} else if strings.HasPrefix(device, "/dev/nvme") {
		partPrefix = fmt.Sprintf("%sp", device)
	} else {
		partPrefix = device
	}

	partNum := 0
	for _, part := range strings.Split(partitionPolicy, ";") {
		partNum += 1
		splitString := strings.SplitN(part, ":", 5)
		if err != nil {
			color.Red("parse part error " + err.Error())
			os.Exit(1)
		}
		if splitString[1] == "efi" {
			command = fmt.Sprintf("parted -s %s mkpart primary fat32 %s %s", device, splitString[2], splitString[3])
		} else {
			command = fmt.Sprintf("parted -s %s mkpart primary %s %s", device, splitString[2], splitString[3])
		}

		if err := runCmd(command, true); err != nil {
			color.Red("run command error: " + command)
			os.Exit(1)
		}

		runCmd("udevadm settle --timeout=15", true)

		partPath := fmt.Sprintf("%s%d", partPrefix, partNum)

		command = fmt.Sprintf("parted -s %s name %d %s", device, partNum, splitString[0])
		if err := runCmd(command, true); err != nil {
			color.Red("run command error: " + command)
			os.Exit(1)
		}
		runCmd("udevadm settle --timeout=15", true)

		if splitString[1] == "efi" {
			if err := Mkfs(partPath, "efi"); err != nil {
				color.Red("mkfs error: " + err.Error())
				return err
			}
			command = fmt.Sprintf("parted -s %s set %d esp on", device, partNum)
			runCmd(command, true)
		} else {
			if err := Mkfs(partPath, splitString[1]); err != nil {
				return err
			}
		}
	}
	return nil
}

func MountPartitions(device string) {
	color.Yellow("Mount partition to /target for installation ...")
	stdout, _, err := captureCmd(fmt.Sprintf("blkid -s PARTLABEL | grep %s", device), true)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	scanner := bufio.NewScanner(strings.NewReader(stdout))
	for scanner.Scan() {
		buf := strings.Split(scanner.Text(), ":")
		blkpath := buf[0]
		blklabel := strings.TrimSpace(buf[1])
		if strings.HasPrefix(blklabel, "PARTLABEL=") {
			blklabel = strings.TrimPrefix(blklabel, "PARTLABEL=")
			blklabel = strings.Trim(blklabel, "\"")

			switch blklabel {
			case "boot", "system", "data":
				{
					relpath := fmt.Sprintf("/target/%s", blklabel)
					if _, err := os.Stat(relpath); err != nil {
						if os.IsNotExist(err) {
							os.MkdirAll(relpath, 0755)
						}
					}
					command := fmt.Sprintf("mount %s %s", blkpath, relpath)
					if err := runCmd(command, true); err != nil {
						color.Cyan(err.Error())
						os.Exit(1)
					}
				}
			default:
				{
					color.White("skip mount " + blkpath)
				}
			}
		}
	}
}

func CheckFilesystem(conf Configuration, cachePath string) error {
	color.Yellow("Checking filesystem ...")
	for _, boot := range conf.Spec.Boots {
		kerPath := fmt.Sprintf("%s/%s", cachePath, boot.Name)
		sha256, err := Sha256File(kerPath)
		if err != nil {
			color.Cyan(err.Error())
			os.Exit(1)
		}
		if sha256 != boot.Sha256sum {
			color.Cyan("checked error", sha256, boot.Sha256sum)
			os.Exit(1)
		}
	}
	for _, bundle := range conf.Spec.Bundles {
		bundlePath := fmt.Sprintf("%s/%s/%s", cachePath, conf.Metadata.Channel, bundle.Name)
		sha256, err := Sha256File(bundlePath)
		if err != nil {
			color.Cyan(err.Error())
			os.Exit(1)
		}
		if sha256 != bundle.Sha256sum {
			color.Cyan("checked error", sha256, bundle.Sha256sum)
			os.Exit(1)
		}
	}
	color.Green("Check filesystem done")
	return nil
}

func InstallFilesystem(conf Configuration, cachePath string) error {
	for _, boot := range conf.Spec.Boots {
		kerPath := fmt.Sprintf("%s/%s", cachePath, boot.Name)
		destPath := fmt.Sprintf("%s/%s/%s", "/target/boot", conf.Metadata.Channel, boot.Name)
		if err := CopyFile(kerPath, destPath); err != nil {
			color.Cyan(err.Error())
			os.Exit(1)
		}
	}
	for _, bundle := range conf.Spec.Bundles {
		bundlePath := fmt.Sprintf("%s/%s/%s", cachePath, conf.Metadata.Channel, bundle.Name)
		destPath := fmt.Sprintf("%s/%s/%s/%s", "/target/system",
			conf.Metadata.Channel, conf.Metadata.Release,
			bundle.Name,
		)
		if err := CopyFile(bundlePath, destPath); err != nil {
			color.Cyan(err.Error())
			os.Exit(1)
		}
	}

	command := fmt.Sprintf("cd /target/system/%s; rm -f current; ln -sf %s current", conf.Metadata.Channel, conf.Metadata.Release)
	runCmd(command, true)

	livekitconf := fmt.Sprintf("channel: %s\n", conf.Metadata.Channel)
	livekitconf += fmt.Sprintf("api-server: %s\n", conf.Config["api_server"])
	livekitconf += fmt.Sprintf("registry-mirror: %s\n", conf.Config["registry_mirror"])
	filePath := fmt.Sprintf("%s/etc/livekit/config.yaml", "/target/data")

	if !PathExist(filepath.Dir(filePath)) {
		os.MkdirAll(filepath.Dir(filePath), 0755)
	}
	fp, _ := os.Create(filePath)
	io.Copy(fp, strings.NewReader(livekitconf))
	fp.Close()

	result, _ := goyaml.Marshal(&conf)
	filePath = fmt.Sprintf("%s/%s/%s/release.yaml",
		"/target/system",
		conf.Metadata.Channel,
		conf.Metadata.Release,
	)
	fp, _ = os.Create(filePath)
	if _, err := io.Copy(fp, strings.NewReader(string(result))); err != nil {
		color.Cyan(err.Error())
		os.Exit(1)
	}
	fp.Close()

	keepFlagPath := fmt.Sprintf("%s/%s/%s/.keep", "/target/system",
		conf.Metadata.Channel, conf.Metadata.Release)
	if err := ioutil.WriteFile(keepFlagPath, []byte("# Keep as origin release by installer\n"), 0644); err != nil {
		color.Cyan(err.Error())
	}
	return nil
}

func InstallBootLoader(conf Configuration) error {
	color.Yellow("installing bootloader ...")
	command := "bootctl --path=/target/boot install"
	runCmd(command, true)

	entry := fmt.Sprintf("title CodeOS %s\n", conf.Metadata.Channel)
	for _, bootfs := range conf.Spec.Boots {
		if strings.HasPrefix(bootfs.Name, "vmlin") {
			entry += fmt.Sprintf("linux /%s/%s\n", conf.Metadata.Channel, bootfs.Name)
		} else if strings.HasPrefix(bootfs.Name, "initr") {
			entry += fmt.Sprintf("initrd /%s/%s\n", conf.Metadata.Channel, bootfs.Name)
		}
	}
	entry += fmt.Sprintf("options %s from=%s/%s\n", conf.Config["boot_options"], conf.Metadata.Channel, conf.Metadata.Release)
	filePath := fmt.Sprintf("%s/loader/entries/%s-%s.conf", "/target/boot", conf.Metadata.Channel, conf.Metadata.Release)

	if !PathExist(filepath.Dir(filePath)) {
		os.MkdirAll(filepath.Dir(filePath), 0755)
	}
	fp, _ := os.Create(filePath)
	defer fp.Close()
	io.Copy(fp, strings.NewReader(entry))
	return nil
}
