package main

import (
	"bytes"
	"os"
	"os/exec"

	"github.com/fatih/color"
)

func runCmd(command string, shell bool) error {
	color.White(command)
	if shell {
		cmd := exec.Command("bash", "-c", command)
		cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
		err := cmd.Run()
		if err != nil {
			color.Red(err.Error())
			return err
		}
		return nil
	}
	cmd := exec.Command(command)
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
	err := cmd.Run()
	if err != nil {
		color.Red(err.Error())
	}
	return nil
}

func captureCmd(command string, shell bool) (string, string, error) {
	var outbuf, errbuf bytes.Buffer
	var cmd *exec.Cmd
	if shell {
		cmd = exec.Command("bash", "-c", command)
	} else {
		cmd = exec.Command(command)
	}
	cmd.Stdout = &outbuf
	cmd.Stderr = &errbuf
	err := cmd.Run()
	stdout := outbuf.String()
	stderr := errbuf.String()

	return stdout, stderr, err
}
