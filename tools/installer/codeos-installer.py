#!/usr/bin/env python3
import yaml
import sys

import argparse
import os
import hashlib
import subprocess
import shutil
import copy

kLive = "/run/initramfs/data"
kTarget = "/target"

class Installer():
    def __init__(self, metadata):
        assert os.path.exists("/sys/firmware/efi/efivars")
        self.cache = os.path.join(kTarget, "system", "cache")
        with open(metadata) as fp:
            self.metadata = yaml.load(fp)

        self.installation_device = None
    
    @staticmethod
    def sha256sum(filename, block_size=65536):
        sha256 = hashlib.sha256()
        with open(filename, 'rb') as f:
            while True:
                block = f.read(block_size)
                if not block:
                    break
                sha256.update(block)
        return sha256.hexdigest()
    
    @staticmethod
    def format_partition(part_path, part_fs):
        if part_fs in ['fat32', 'efi']:
            command = "mkfs.vfat -F32 %s" % part_path
        elif part_fs in ['reiserfs']:
            command = "mkfs.%s -f %s" % (part_fs, part_path)
        elif part_fs in ['ext2', 'ext3', 'ext4']:
            command = "mkfs -t %s -F %s" % (part_fs, part_path)
        else:
            print("May unsupport create %s at %s" % (part_fs, part_path))
            command = "mkfs -t %s %s" % (part_fs, part_path)

        status, _ = subprocess.getstatusoutput(command)
        if status != 0:
            print("Failed to create partition. %s" % command)
            sys.exit(1)

    def prepare_partitions(self):
        self.select_device_list()
        self.check_device_size()
        if not self.check_partitions_prepared():
            print(" -> Partition has not prepared.")
            print("    We will format your disk before installation")
            self.format_partitions()

        self.mount_partition()

    def select_device_list(self):
        device_list = []
        for path in os.listdir("/dev/disk/by-id/"):
            if '-part' not in path:
                relpath = os.path.realpath(os.path.join("/dev/disk/by-id/%s" % path))
                if relpath not in device_list:
                    device_list.append(relpath)

        print("Listing Disk for installtion.")
        for index,device in enumerate(device_list): 
            print("%s: %s" % (index, device))

        print("Select disk for installation: ")
        num = input()

        self.installation_device = device_list[int(num)]

    def check_device_size(self):
        print("Check device size ...")
        device_size = subprocess.getoutput("blockdev --getsize64 %s" % self.installation_device)
        if int(device_size) / 1048576 / 1024 < self.metadata['config']['minimum_device_size']:
            print(" ✗ Mininum Size is not required.")
            sys.exit(1)
    
    def check_partitions_prepared(self):
        print("Check partition prepared ...")
        for label in ['boot', 'system', 'data']:
            cmd = "blkid -s PARTLABEL | grep '^%s' | grep -q 'PARTLABEL=\"%s\"'" % \
                (self.installation_device, label)
            status, _  = subprocess.getstatusoutput(cmd)
            if status != 0:
                print(" ✗ Partition is not prepared.")
                return False
        print(" ✓ Partition has prepared")
        return True
    
    def format_partitions(self):
        print(" * Create new gpt table")
        status, _ = subprocess.getstatusoutput("parted -s %s mktable gpt" % self.installation_device)
        if status != 0:
            print("  Failed to create gpt lable")
            sys.exit(1)

        device_size = int(subprocess.getoutput("blockdev --getsize64 %s" % self.installation_device))
        root_size = int(device_size / 1048576 * 0.5)

        partition_policy = "boot:efi:1Mib:512Mib;system:f2fs:512Mib:%dMib;data:f2fs:%dMib:100%%" %(int(root_size), int(root_size))
        
        if self.installation_device.startswith('/dev/mmc'):
            part_prefix = "%sp" % self.installation_device
        elif self.installation_device.startswith('/dev/nvme'):
            part_prefix = "%sp" % self.installation_device
        else:
            part_prefix = self.installation_device

        part_num = 0
        for part in partition_policy.split(';'):
            part_num += 1
            part_path = "%s%d" % (part_prefix, part_num)
            part = part.split(':')
            part_label = part[0]
            part_fs = part[1]
            part_start = part[2]
            part_end = part[3]
            print(" * create %s partition with %s - %s to %s" % (
                part_label, part_fs, part_start, part_end
                ))
            if part_label == "boot":
                print("    create efi boot partition")
                command = "parted -s %s mkpart primary fat32 %s %s" % (self.installation_device, part_start, part_end)
            else:
                command = "parted -s %s mkpart primary %s %s" % (self.installation_device, part_start, part_end)

            status, _ = subprocess.getstatusoutput(command)
            if status != 0:
                print("    failed to mkpart. - %s" % command)
                sys.exit(1)
            
            print("    set %s label name to %s" %(part_path, part_label))
            command = "parted -s %s name %d %s" % (self.installation_device, part_num, part_label)
            status, _ = subprocess.getstatusoutput(command)
            if status != 0:
                print("    failed to set partbel.")
                sys.exit(1)
            subprocess.call("udevadm settle --timeout=15", shell=True)

            if part_label == "boot":
                self.format_partition(part_path, 'fat32')
                subprocess.call("parted -s %s set %d esp on" % (self.installation_device, part_num), shell=True)
            else:
                self.format_partition(part_path, part_fs)

            subprocess.call("udevadm settle --timeout=15", shell=True)

    def mount_partition(self):
        print("Mount partitions to /target for installation ...")
        if not os.path.exists("/target"):
            os.makedirs("/target")

        command = 'blkid -s PARTLABEL | grep "^%s"' % self.installation_device
        status, output = subprocess.getstatusoutput(command)
        output = output.split('\n')
        for label in ['boot', 'system', 'data']:
            for line in output:
                if "PARTLABEL=\"%s\"" % label in line:
                    part_path = line.split(':')[0]
                    part_dest = os.path.join('/target', label)
                    subprocess.call("mkdir -p %s" % part_dest, shell=True)
                    status, _ = subprocess.getstatusoutput("mount %s %s" % (part_path, part_dest))
                    if status != 0:
                        print("   failed to mount partition.")
                        os.exit(1)

    def generate_release_yaml(self):
        print("Generate release.yaml")
        metadata = copy.deepcopy(self.metadata)
        for bundle in metadata['spec']['bundles']:
            if 'rdiffs' in bundle:
                bundle.pop('rdiffs')

        with open(os.path.join(self.cache, 'release.yaml'), 'w') as fp:
            fp.write(yaml.dump(metadata, default_flow_style=False))


    def install_filesystem(self):

        print("Check before start ...")
        if not os.path.exists(self.cache):
            os.makedirs(self.cache)
        
        self.bundles = self.metadata['spec']['bundles']
        self.channel = self.metadata['metadata']['channel']
        self.release = str(self.metadata['metadata']['release'])

        if os.path.exists(os.path.join(kTarget, "system", self.channel, self.release)):
            print("✓ filesystem has already installed.")
            sys.exit(0)

        self.check_origin_bundles()
        self.generate_release_yaml()
        self.install_boot_kernel()
        self.update_currrent_symlink()

    def check_origin_bundles(self):
        print("Check origin bundles ...")
        
        for bundle in self.bundles:
            if bundle.get('source') is None:
                bundle['source'] = bundle['name']

            if not os.path.exists(os.path.join(kLive, self.channel, bundle['source'])):
                print("  ✗ source bundle %s is not exist" % bundle['source'])
                sys.exit(1)
            
            shutil.copy(os.path.join(kLive, self.channel, bundle['source']),
                os.path.join(self.cache, bundle['name'])
                )

            sha256sum = self.sha256sum(os.path.join(self.cache, bundle['name']))
            if sha256sum != bundle['sha256sum']:
                print("  ✗ %s ... FAILED" % bundle['source'])
                os.unlink(os.path.join(self.cache, bundle['name']))
                sys.exit(1)
            else:
                print("  ✓ %s ... PASSED" % bundle['source'])

    def install_boot_kernel(self):
        assert os.path.isdir(os.path.join(kTarget, 'boot'))
        print("Update EFI boot manager")
        status, _ = subprocess.getstatusoutput("bootctl --path=/target/boot install")
        #subprocess.call("sed -i 's|^#timeout\s.*$|timeout 3|g' /target/boot/loader/loader.conf", shell=True)
        if status != 0:
            print(" Install bootloader to esp partition failed.")
        
        print("Install boot kernel")
        if not os.path.exists(os.path.join(kTarget, 'boot', self.channel)):
            os.makedirs(os.path.join(kTarget, 'boot', self.channel))
        for bootfs in self.metadata['spec']['boots']:
            shutil.copy(os.path.join(kLive, bootfs['name']), os.path.join(self.cache, bootfs['name']))
            sha256 = self.sha256sum(os.path.join(self.cache, bootfs['name']))
            if sha256 != bootfs['sha256sum']:
                print("  ✗ %s ... FAILED" % bootfs['name'])
                os.unlink(os.path.join(os.path.join(self.cache, bootfs['name'])))
                sys.exit(1)
            else:
                print("  ✓ %s ... PASSED" % bootfs['name'])
                shutil.move(
                    os.path.join(self.cache, bootfs['name']), 
                    os.path.join(kTarget, 'boot', self.channel, bootfs['name'])
                    )

        if not os.path.exists(os.path.join(kTarget, 'boot/loader/entries')):
            os.makedirs(os.path.join(kTarget, 'boot/loader/entries'))

        entry =  "title CodeOS %s\n" % self.channel
        for bootfs in self.metadata['spec']['boots']:
            if bootfs['name'].startswith('vmlin'):
                entry += "linux /%s/%s\n" % (self.channel, bootfs['name'])
            if bootfs['name'].startswith('initr'):
                entry += "initrd /%s/%s\n" % (self.channel, bootfs['name'])
        entry += "options %s from=%s/%s\n" % (self.metadata['config']['boot_options'], str(self.channel), self.release)
        with open(os.path.join(kTarget, 'boot/loader/entries/%s-%s.conf' % (self.channel, self.release)), "w") as fp:
            fp.write(entry)
                

    def update_currrent_symlink(self):
        print("Move cache to release ...")
        os.makedirs(os.path.join(os.path.dirname(self.cache), self.channel), exist_ok=True)
        oldpwd = os.getcwd()
        os.chdir(os.path.join(os.path.dirname(self.cache), self.channel))
        shutil.move(self.cache, self.release)
        print("Create current symbol link ...")
        if os.path.exists('current'):
            os.unlink('current')
        os.symlink(self.release, 'current')
        with open(os.path.join(self.release, '.keep'), 'w') as fp:
            fp.write("# keep as origin release by installer\n")
        os.chdir(oldpwd)
        print("Install successful.")


class Main():

    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Installer for codeos with ota support.',
            usage='''codeos-installer <command> [<args>]
''')
        
        parser.add_argument('command', help='Subcommand to run')
        # parse_args defaults to [1:] for args, but you need to
        # exclude the rest of the args too, or validation will fail

        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)
        getattr(self, args.command)()
    
    def prepare(self):
        parser = argparse.ArgumentParser(
            description='Show changes to the filesystem')
        parser.add_argument('--metadata', default="/run/initramfs/data/release.yaml")
        parser.add_argument('--force', action='store_true')
        args = parser.parse_args(sys.argv[2:])
        Installer(args.metadata).prepare_partitions()

    def install(self):
        parser = argparse.ArgumentParser(
            description='Show changes to the filesystem')
        parser.add_argument('--metadata', default="/run/initramfs/data/release.yaml")
        parser.add_argument('--force', action='store_true')
        args = parser.parse_args(sys.argv[2:])
        Installer(args.metadata).install_filesystem()

if __name__ == "__main__":
    Main()
