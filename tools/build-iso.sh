#!/bin/bash

GENEFIIMG(){
	rm -rf binary/boot/efi.img binary/efi-temp
	mkdir -p binary/efi-temp/loader/entries binary/boot
	mkdir -p binary/efi-temp/EFI/BOOT
	curl -o binary/efi-temp/EFI/BOOT/BOOTX64.EFI http://192.168.0.123:5211/ota/misc/EFI/BOOT/BOOTX64.EFI

	cat > binary/efi-temp/loader/entries/live.conf << EOF
title CodeOS Live
linux /vmlinuz
initrd /initrd.img
options from=catty quiet splash boot=live livekit.live
EOF
	cp vmlinuz* binary/efi-temp/vmlinuz
	cp initrd.img* binary/efi-temp/initrd.img

	_TOTALSIZE=$(du -sk binary/efi-temp/ | awk '{print $1}')
	# Add 5% safety margin
	_TOTALSIZE=$(( $_TOTALSIZE * 21 / 20 ))
	# Required size rounded to upper 32kb
	_BLOCKS=$(( ($_TOTALSIZE + 31) / 32 * 32 ))
	echo "EFI boot image needs $_TOTALSIZE Kb, thus allocating $_BLOCKS blocks."

	mkfs.msdos -C binary/boot/efi.img ${_BLOCKS} >/dev/null
	mcopy -s -v -i binary/boot/efi.img binary/efi-temp/* ::
}

GENISO(){
	rm -rf $1/binary/efi-temp
	XORRISO_OPTIONS="-R -r -J -joliet-long -l -cache-inodes -quiet"
	XORRISO_OPTIONS="${XORRISO_OPTIONS} -eltorito-alt-boot -e boot/efi.img -no-emul-boot"
	XORRISO_OPTIONS="${XORRISO_OPTIONS} -append_partition 2 0x01 $1/boot/efi.img"
	xorriso -as mkisofs ${XORRISO_OPTIONS} -exclude "rdiffs" -o result.iso $1
}

case $1 in
	efi)
		GENEFIIMG ;;
	iso)
		GENISO $2;;
esac
