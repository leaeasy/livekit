package main

import (
	"io"
	"io/ioutil"
	"log"
	"os"

	"github.com/akamensky/argparse"
	"github.com/marcsauter/single"
	"launchpad.net/goyaml"
)

const (
	kLive     = "/run/initramfs/data"
	kConfig   = "/etc/livekit/config.yaml"
	kCache    = "/run/initramfs/data/cache"
	kLogFile  = "/var/log/codeos-updater.log"
	kPipeFile = "/tmp/codeos-updater.pipe"
)

var pipefp *os.File

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.LUTC)

	parser := argparse.NewParser("CodeOS Updater", "CodeOS OTA Updater Tools")
	downloadCmd := parser.NewCommand("download", "Download OTA bundle to Cache")
	updateCmd := parser.NewCommand("update", "Commit OTA update to Filesystem")

	var yamlfile *string = parser.String("m", "metadata",
		&argparse.Options{Required: true, Help: "metadata is a required release.yaml file."})

	if err := parser.Parse(os.Args); err != nil {
		log.Fatal(err)
	}

	s := single.New("codeos-updater")
	if err := s.CheckLock(); err != nil && err == single.ErrAlreadyRunning {
		log.Fatal("another instance of the app is already running, exiting")
	} else if err != nil {
		log.Fatalf("failed to acquire exclusive app lock: %v", err)
	}
	defer s.TryUnlock()

	var err error
	pipefp, err = OpenPipeFile(kPipeFile)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		if err := pipefp.Close(); err != nil {
			log.Fatal("close pipefile error:", err)
		}
		os.Remove(kPipeFile)
	}()

	logfile, err := os.OpenFile(kLogFile,
		os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)

	if err != nil {
		log.Fatal("open log file error - ", err)
	}
	defer logfile.Close()

	logger := io.MultiWriter(os.Stdout, logfile)
	log.SetOutput(logger)

	var livekitconf LiveKitConfig
	if PathExist(kConfig) {
		log.Println("loading configure", kConfig)
		bytes, _ := ioutil.ReadFile(kConfig)
		goyaml.Unmarshal(bytes, &livekitconf)
	} else {
		log.Println("initial configure as default")
		livekitconf = NewLiveKitConfig()
	}

	var conf Configuration
	bytes, err := ioutil.ReadFile(*yamlfile)
	if err != nil {
		log.Fatal(err)
	}
	goyaml.Unmarshal(bytes, &conf)

	if downloadCmd.Happened() {

		var downloads = make(map[string]uint64)
		if conf.Install.UpdateKernel {
			for _, boot := range conf.Spec.Boots {
				downloads[boot.Name] = boot.Size
			}
		}
		for _, bundle := range conf.Spec.Bundles {
			for file, size := range bundle.getDownloads() {
				downloads[file] = size
			}
		}

		var downloadSize uint64 = 0

		log.Println("Download details: ")
		for file, size := range downloads {
			log.Println(" - "+file+":", size)
			downloadSize += size
		}
		log.Println("Total download size:", downloadSize)

		log.Println("Start download codeos ota bundles...")
		if !PathExist(kCache) {
			if err := os.MkdirAll(kCache, os.ModePerm); err != nil {
				log.Fatal(err)
			}
		}

		done := make(chan bool)
		go PrintDownloadPercent(done, downloads, downloadSize)
		if conf.Install.UpdateKernel {
			log.Println("Getting lastest kernel ...")
			for _, boot := range conf.Spec.Boots {
				if err := boot.Download(livekitconf.RegistryMirror,
					conf.Metadata); err != nil {
					log.Fatal(err)
				}
				if !boot.Check() {
					log.Fatal("Check " + boot.Sha256sum + " failed.")
				}
			}
		}
		log.Println("Getting bundles ...")
		for _, bundle := range conf.Spec.Bundles {
			err := bundle.Download(livekitconf.RegistryMirror, conf.Metadata)
			if err != nil {
				log.Fatal(err)
			}
		}

		close(done)

	} else if updateCmd.Happened() {
		log.Println("Start upgrade ota to filesystem ...")
		for _, bundle := range conf.Spec.Bundles {
			if bundle.Method == "patch" {
				if err := bundle.Patch(conf.Metadata.Channel, conf.Metadata.Origin); err != nil {
					log.Fatal(err)
				}
			}
			if !bundle.Check() {
				log.Fatal("Check " + bundle.Name + " failed.")
			}
		}

		log.Println("Installing filesystem ...")
		for _, bundle := range conf.Spec.Bundles {
			if err := bundle.Install(conf.Metadata.Channel, conf.Metadata.Release); err != nil {
				log.Fatal(err)
			}
		}

		if conf.Install.UpdateKernel {
			log.Println("Upgrading kernel ...")
			for _, boot := range conf.Spec.Boots {
				if err := boot.Install(conf.Metadata.Channel, conf.Metadata.Release); err != nil {
					log.Fatal(err)
				}
			}
		}

		if err := conf.GenRelease(); err != nil {
			log.Fatal(err)
		}
		if err := conf.UpdateCurrent(); err != nil {
			log.Fatal(err)
		}
		if err := conf.UpdateBootloader(); err != nil {
			log.Fatal(err)
		}
		if err := conf.RemoveUnusedFileSystem(); err != nil {
			log.Fatal(err)
		}

		log.Println("Clean filesystem cache")
		os.RemoveAll(kCache)
	} else {
		log.Println("Please use download or update as action")
		os.Exit(0)
	}
}
