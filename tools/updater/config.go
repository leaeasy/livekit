package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"

	"launchpad.net/goyaml"
)

type LiveKitConfig struct {
	Channel        string `yaml:"channel"`
	ApiServer      string `yaml:"api-server"`
	RegistryMirror string `yaml:"registry-mirror"`
}

type Install struct {
	UpdateKernel bool `yaml:"update-kernel"`
}

type Configuration struct {
	Metadata Metadata
	Config   map[string]string
	Spec     Spec
	Install  Install
}

type Metadata struct {
	Channel    string
	Release    string
	Version    string
	Build      string
	Manipulate string
	Publisher  string
	Origin     string
}

type Spec struct {
	Boots   []*Boot   `yaml:"boots"`
	Bundles []*Bundle `yaml:"bundles"`
}

type Bundle struct {
	Method    string
	Name      string
	Sha256sum string
	Rdiffs    []*Rdiff `yaml:"rdiffs"`
	Size      uint64
}

type Rdiff struct {
	Origin    string
	Sha256sum string
	Size      uint64
}

type Boot struct {
	Name      string
	Sha256sum string
	Size      uint64
}

type FileSystemConfiguration struct {
	Version  int
	Metadata map[string]string
	Spec     FsSpec
	Config   map[string]string
}

type FsSpec struct {
	Boots   []*Boot     `yaml:"boots"`
	Bundles []*FsBundle `yaml:"bundles"`
}

type FsBundle struct {
	Name      string
	Sha256sum string
}

func NewLiveKitConfig() LiveKitConfig {
	return LiveKitConfig{
		Channel:        "catty",
		ApiServer:      "http://192.168.0.123:8000/api",
		RegistryMirror: "http://192.168.0.123:8000/ota",
	}
}

func (conf *Configuration) UpdateBootloader() error {
	if err := MountBootPartition(); err != nil {
		return err
	}
	entry := fmt.Sprintf("title CodeOS %s\n", conf.Metadata.Channel)
	for _, boot := range conf.Spec.Boots {
		if strings.HasPrefix(boot.Name, "initr") {
			entry += fmt.Sprintf("initrd /%s/%s\n", conf.Metadata.Channel, boot.Name)
		} else if strings.HasPrefix(boot.Name, "vmlin") {
			entry += fmt.Sprintf("linux /%s/%s\n", conf.Metadata.Channel, boot.Name)
		}
	}
	entry += fmt.Sprintf("options %s\n", conf.Config["boot_options"])

	if !PathExist("/boot/loader/entries") {
		os.MkdirAll("/boot/loader/entries", 0755)
	}

	filepath := fmt.Sprintf("/boot/loader/entries/%s-%s.conf", conf.Metadata.Channel, conf.Metadata.Release)
	fp, err := os.Create(filepath)
	if err != nil {
		return err
	}

	defer fp.Close()

	_, err = io.Copy(fp, strings.NewReader(entry))
	if err != nil {
		return err
	}

	return nil
}

func (conf *Configuration) UpdateCurrent() error {
	log.Println("update current filesystem symbol link")
	curdir, _ := os.Getwd()
	cache := fmt.Sprintf("%s/%s", kLive, conf.Metadata.Channel)
	os.Chdir(cache)
	defer os.Chdir(curdir)
	if PathExist("current") {
		os.Remove("current")
	}
	return os.Symlink(conf.Metadata.Release, "current")
}

func (conf *Configuration) GenRelease() (err error) {
	fsconfig := FileSystemConfiguration{}
	metadata := conf.Metadata
	fsconfig.Metadata = map[string]string{
		"build": metadata.Build, "channel": metadata.Channel,
		"release": metadata.Release, "publisher": metadata.Publisher,
		"version": metadata.Version,
	}
	fsconfig.Spec.Boots = conf.Spec.Boots
	fsconfig.Config = conf.Config
	for _, bundle := range conf.Spec.Bundles {
		fsbundle := &FsBundle{
			Name:      bundle.Name,
			Sha256sum: bundle.Sha256sum,
		}
		fsconfig.Spec.Bundles = append(fsconfig.Spec.Bundles, fsbundle)
	}

	result, _ := goyaml.Marshal(&fsconfig)

	filepath := fmt.Sprintf("%s/%s/%s/release.yaml", kLive, conf.Metadata.Channel, conf.Metadata.Release)
	fp, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer fp.Close()

	_, err = io.Copy(fp, strings.NewReader(string(result)))
	if err != nil {
		return err
	}
	return nil
}

func (conf *Configuration) RemoveUnusedFileSystem() error {
	log.Println("Cleaning used filesytem ...")
	var unUsedFS []string
	var keepedFS []string
	cachepath := fmt.Sprintf("%s/%s", kLive, conf.Metadata.Channel)

	fileinfo, _ := ioutil.ReadDir(cachepath)
	for _, file := range fileinfo {
		if file.IsDir() && PathExist(fmt.Sprintf("%s/%s/release.yaml", cachepath, file)) {
			if file.Name() == conf.Metadata.Release || PathExist(fmt.Sprintf("%s/%s/.keep", cachepath, file)) {
				keepedFS = append(keepedFS, file.Name())
			} else {
				unUsedFS = append(unUsedFS, file.Name())
			}
		}
	}

	loaderpath := "/boot/loader/entries"
	bootinfo, _ := ioutil.ReadDir(loaderpath)
	var unUsedLoaders []string
	var flag bool
	for _, file := range bootinfo {
		if strings.HasPrefix(file.Name(), fmt.Sprintf("%s-", conf.Metadata.Channel)) {
			flag = false
			for _, keep := range keepedFS {
				if file.Name() == fmt.Sprintf("%s-%s.conf", conf.Metadata.Channel, keep) {
					flag = true
					continue
				}
			}
			if !flag {
				unUsedLoaders = append(unUsedLoaders, file.Name())
			}
		}
	}
	var kernelMap map[string]bool
	kernelMap = make(map[string]bool)
	fileinfo, _ = ioutil.ReadDir(fmt.Sprintf("/boot/%s", conf.Metadata.Channel))
	for _, file := range fileinfo {
		if strings.HasPrefix(file.Name(), "initr") || strings.HasPrefix(file.Name(), "vmlin") {
			kernelMap[file.Name()] = false
		}
	}

	for _, fs := range keepedFS {
		loaderconf, err := ParserLoaderConf(fmt.Sprintf("/boot/loader/entries/%s-%s.conf", conf.Metadata.Channel, fs))
		if err != nil {
			log.Fatal(err)
			continue
		}
		if _, ok := loaderconf["linux"]; ok {
			basepath := path.Base(loaderconf["linux"])
			kernelMap[basepath] = true
		}
		if _, ok := loaderconf["initrd"]; ok {
			basepath := path.Base(loaderconf["initrd"])
			kernelMap[basepath] = true
		}

	}

	for kernel, flag := range kernelMap {
		if !flag {
			kernelpath := fmt.Sprintf("/boot/%s/%s", conf.Metadata.Channel, kernel)
			log.Println("deleting " + kernelpath)
			os.Remove(kernelpath)
		}
	}

	for _, loader := range unUsedLoaders {
		loaderpath := fmt.Sprintf("/boot/loader/entries/%s", loader)
		log.Println("deleting " + loaderpath)
		os.Remove(loaderpath)
	}
	for _, fs := range unUsedFS {
		fspath := fmt.Sprintf("%s/%s", cachepath, fs)
		log.Println("deleting " + fspath)
		os.RemoveAll(fspath)
	}

	return nil
}
