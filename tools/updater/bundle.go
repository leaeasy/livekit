package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"sort"
)

func (b *Bundle) Check() bool {
	log.Println("checking " + b.Name)
	dest := fmt.Sprintf("%s/%s", kCache, b.Name)
	return CheckSha256Sum(dest, b.Sha256sum)
}

func (b *Bundle) getDownloads() map[string]uint64 {
	downloads := make(map[string]uint64)
	switch b.Method {
	case "patch":
		{
			for _, rdiff := range b.Rdiffs {
				downloads[rdiff.Sha256sum] = rdiff.Size
			}
		}
	case "remote", "":
		{
			downloads[b.Name] = b.Size
		}
	}
	return downloads
}

func (b *Bundle) Download(url string, metadata Metadata) error {
	log.Println("Download method Bundle: " + b.Name)
	channel := metadata.Channel
	release := metadata.Release
	switch b.Method {
	case "patch":
		{
			for _, rdiff := range b.Rdiffs {
				rdiff.Download(url, channel, rdiff.Origin)
			}
			return nil
		}
	case "remote", "":
		{
			fileurl := fmt.Sprintf("%s/%s/%s/%s/%s", url, channel, release, channel, b.Name)
			dest := fmt.Sprintf("%s/%s", kCache, b.Name)
			return Download(fileurl, dest)
		}
	case "local":
		{
			origin := metadata.Origin
			fileurl := fmt.Sprintf("%s/%s/%s/%s", kLive, channel, origin, b.Name)
			dest := fmt.Sprintf("%s/%s", kCache, b.Name)
			if PathExist(dest) {
				return nil
			}
			return os.Link(fileurl, dest)
		}
	}
	return nil
}

func (b *Bundle) Patch(channel string, release string) error {
	log.Println("Patching " + b.Name)
	rdiffs := b.Rdiffs
	sort.Slice(rdiffs[:], func(i, j int) bool {
		return rdiffs[i].Origin < rdiffs[j].Origin
	})
	for _, rdiff := range rdiffs {
		if err := rdiff.Patch(channel, release, b.Name); err != nil {
			log.Fatal(err)
		}
	}
	destfile := fmt.Sprintf("%s/%s", kCache, b.Name)
	if !PathExist(destfile + ".temp") {
		return errors.New(destfile + " not exists.")
	}
	os.Rename(destfile+".temp", destfile)
	return nil
}

func (b *Bundle) Install(channel string, release string) error {
	origfile := fmt.Sprintf("%s/%s", kCache, b.Name)
	destfile := fmt.Sprintf("%s/%s/%s/%s", kLive, channel, release, b.Name)
	return MoveFile(origfile, destfile)
}

func (r *Rdiff) Download(url string, channel string, release string) error {
	fileurl := fmt.Sprintf("%s/%s/%s/%s/%s", url, channel, release, "rdiffs", r.Sha256sum)
	dest := fmt.Sprintf("%s/%s", kCache, r.Sha256sum)
	return Download(fileurl, dest)
}

func (r *Rdiff) Check() bool {
	dest := fmt.Sprintf("%s/%s", kCache, r.Sha256sum)
	return CheckSha256Sum(dest, r.Sha256sum)
}

func (r *Rdiff) Patch(channel string, release string, bundle string) error {
	// found the basis in cache directory
	basis := fmt.Sprintf("%s/%s.temp", kCache, bundle)
	if !PathExist(basis) {
		basis = fmt.Sprintf("%s/%s/%s/%s", kLive, channel, release, bundle)
		if !PathExist(basis) {
			return errors.New(basis + " not exists.")
		}
	}
	log.Println("Patching " + basis + " with " + r.Sha256sum)

	delta := fmt.Sprintf("%s/%s", kCache, r.Sha256sum)
	tempfile := fmt.Sprintf("%s/.%s.%s", kCache, bundle, r.Origin)

	args := []string{"patch", basis, delta, tempfile}
	cmd := exec.Command("rdiff", args...)
	if err := cmd.Run(); err != nil {
		log.Println(err)
		return err
	}

	newfile := fmt.Sprintf("%s/%s.temp", kCache, bundle)
	if err := os.Rename(tempfile, newfile); err != nil {
		log.Println(err)
		return err
	}

	return nil
}
