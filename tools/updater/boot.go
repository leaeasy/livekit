package main

import (
	"fmt"
)

func (b *Boot) Check() bool {
	dest := fmt.Sprintf("%s/%s", kCache, b.Name)
	return CheckSha256Sum(dest, b.Sha256sum)
}

func (b *Boot) Download(url string, metadata Metadata) error {
	channel := metadata.Channel
	release := metadata.Release
	fmt.Println("Download method Boot: " + b.Name)
	fileurl := fmt.Sprintf("%s/%s/%s/%s", url, channel, release, b.Name)
	dest := fmt.Sprintf("%s/%s", kCache, b.Name)
	return Download(fileurl, dest)
}

func (b *Boot) Install(channel string, release string) error {
	origfile := fmt.Sprintf("%s/%s", kCache, b.Name)
	destfile := fmt.Sprintf("/boot/%s/%s", channel, b.Name)
	return CopyFile(origfile, destfile)
}
