package main

import (
	"bufio"
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"
	"time"
)

func PathExist(path string) bool {
	_, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}

func OpenPipeFile(filename string) (fp *os.File, err error) {
	os.Remove(filename)
	if err = syscall.Mkfifo(filename, 0666); err != nil {
		return nil, err
	}
	fp, err = os.OpenFile(filename, os.O_RDWR, os.ModeNamedPipe)
	if err != nil {
		return nil, err
	}
	return fp, nil
}

func PrintDownloadPercent(done chan bool, downloads map[string]uint64, total uint64) {
	for {
		select {
		case <-done:
			{
				log.Println("Download filesystem done.")
				return
			}
		default:
			{
				var finish uint64 = 0
				for name := range downloads {
					path := fmt.Sprintf("%s/%s", kCache, name)
					if PathExist(path) {
						file, _ := os.Open(path)
						fi, _ := file.Stat()
						size := fi.Size()
						finish += uint64(size)
					} else if PathExist(fmt.Sprintf("%s.part", path)) {
						file, _ := os.Open(fmt.Sprintf("%s.part", path))
						fi, _ := file.Stat()
						size := fi.Size()
						finish += uint64(size)
					}
				}
				percent := float64(finish) / float64(total) * 100
				pipefp.WriteString(fmt.Sprintf("\rDownload Progressing (%0.f%%)", percent))
			}
		}
		time.Sleep(time.Second)
	}
}

func PrintHashsumPercent(finish chan uint64, total uint64) {
	for {
		_finish := <-finish
		percent := float64(_finish) / float64(total) * 100
		pipefp.WriteString(fmt.Sprintf("\rCheck Progressing (%0.f%%)", percent))
		if _finish >= total {
			break
		}
	}
}

func Sha256File(filePath string) (string, error) {
	var hashValue string
	file, err := os.Open(filePath)
	if err != nil {
		return hashValue, err
	}
	defer file.Close()

	const filechunk = 8192

	fi, err := file.Stat()
	filesize := fi.Size()

	blocks := uint64(math.Ceil(float64(filesize) / float64(filechunk)))

	hash := sha256.New()
	finish := make(chan uint64)
	go PrintHashsumPercent(finish, blocks)

	for i := uint64(0); i < blocks; i++ {
		blocksize := int(math.Min(filechunk, float64(filesize-int64(i*filechunk))))
		finish <- i
		buf := make([]byte, blocksize)
		file.Read(buf)
		io.WriteString(hash, string(buf))
	}

	finish <- blocks
	fmt.Printf("%s checksum is %x\n", file.Name(), hash.Sum(nil))
	hashInBytes := hash.Sum(nil)
	hashValue = hex.EncodeToString(hashInBytes)
	return hashValue, nil
}

func MoveFile(source string, destination string) error {
	log.Println("installing " + source + " to " + destination)
	if err := os.MkdirAll(filepath.Dir(destination), os.ModePerm); err != nil {
		return err
	}
	if err := os.Rename(source, destination); err != nil {
		return err
	}
	return nil
}

func CopyFile(source string, destination string) error {
	log.Println("copying " + source + " to " + destination)
	reader, err := os.Open(source)

	if err != nil {
		return err
	}

	defer reader.Close()

	if err := os.MkdirAll(filepath.Dir(destination), os.ModePerm); err != nil {
		return err
	}

	writer, err := os.Create(destination)
	if err != nil {
		return err
	}

	defer writer.Close()

	_, err = io.Copy(writer, reader)
	if err != nil {
		return err
	}

	err = writer.Sync()
	if err != nil {
		return err
	}

	return nil
}

func CheckSha256Sum(filepath string, filehash string) bool {
	if !PathExist(filepath) {
		log.Fatal(filepath + " is not exists")
		return false
	}
	sha256sum, err := Sha256File(filepath)
	if err != nil {
		fmt.Println(err)
		return false
	}
	if sha256sum != filehash {
		return false
	}
	return true
}

func Download(fileurl string, dest string) error {
	if PathExist(dest) {
		return nil
	}
	var command []string
	dlagent := fmt.Sprintf("/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -o %s.part %s", dest, fileurl)
	command = strings.Split(dlagent, " ")
	fmt.Println("Downloading " + fileurl + " -> " + dest)
	cmd := exec.Command(command[0], command[1:]...)
	cmd.Stdout, cmd.Stderr = os.Stdout, os.Stderr
	if err := cmd.Run(); err != nil {
		fmt.Println(err)
		return err
	}
	// rename the temporary download file to the final destination
	if err := os.Rename(dest+".part", dest); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func MountBootPartition() error {
	log.Println("Prepare mounting boot partition ...")
	if !PathExist("/dev/disk/by-partlabel/boot") {
		return errors.New("Boot partition is not exists")
	}

	blkpath, _ := filepath.EvalSymlinks("/dev/disk/by-partlabel/boot")
	fp, err := os.Open("/proc/mounts")
	if err != nil {
		return err
	}
	defer fp.Close()

	s := bufio.NewScanner(fp)
	var (
		mountblk   string
		mountpoint string
	)
	for s.Scan() {
		text := s.Text()
		if _, err := fmt.Sscanf(text, "%s %s", &mountblk, &mountpoint); err != nil {
			return err
		}
		if mountpoint == "/boot" {
			return nil
		}
	}

	cmd := exec.Command("mount", blkpath, "/boot")
	if err := cmd.Run(); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func ParserLoaderConf(loader string) (result map[string]string, err error) {
	result = make(map[string]string)
	f, err := os.Open(loader)
	if err != nil {
		return result, err
	}
	defer f.Close()

	buf := bufio.NewReader(f)
	line, err := buf.ReadBytes('\n')
	for err == nil {
		line = bytes.TrimRight(line, "\n")
		if len(line) > 0 {
			if line[len(line)-1] == 13 {
				line = bytes.TrimRight(line, "\r")
			}
			_line := string(line)
			switch {
			case strings.HasPrefix(_line, "title"):
				{
					result["title"] = strings.SplitN(_line, " ", 2)[1]
				}
			case strings.HasPrefix(_line, "initrd"):
				{
					result["initrd"] = strings.SplitN(_line, " ", 2)[1]
				}
			case strings.HasPrefix(_line, "linux"):
				{
					result["linux"] = strings.SplitN(_line, " ", 2)[1]
				}
			case strings.HasPrefix(_line, "options"):
				{
					result["opitons"] = strings.SplitN(_line, " ", 2)[1]
				}
			}
		}
		line, err = buf.ReadBytes('\n')
	}
	return result, nil
}
